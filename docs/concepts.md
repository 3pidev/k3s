# Our Concepts

## Code Branching
We used code branching and tagging to automatically deploy your code to the correct namespace. We use 3 different branches:
* dev The development branch
* stag The staging branch
* master The production branch

## Domain and Namespace prefixes
As you probably have already noticed that we use domain and namespace prefixes to segregate/isolate between the different stages of usages.

##### Domain prefixes
* .sys.*&lt;your-domain&gt;* is used for **system** functions, normally accessed by an administrator
* .com.*&lt;your-domain&gt;* is used for **common** functions, normally accessed by many users and not specific for one application
* .iam.*&lt;your-domain&gt;* is used for **Identity and Access Management**
* *&lt;your-project&gt;*.dev.*your-domain&gt;* is used to deploy the **development** branch
* *&lt;your-project&gt;*.stg.&lt;*your-domain&gt;* is used to deploy the **staging** branch
* *&lt;your-project&gt;*.&lt;*your-domain&gt;* is used to deploy the **master/production** branch
* api.*&lt;your-project&gt;*.*&lt;stage&gt;.&lt;your-domain&gt;* is used for access to **backend**, for which we use krakend api gateway

##### Namespace prefixes
* &lt;your-project&gt;*-dev* for the **development** branch
* &lt;your-project&gt;*-stg* for the **staging** branch
* &lt;your-project&gt;*-prod* for the **production** branch
* &lt;your-project&gt;*-&lt;stage&gt;-api* for the **backend** services
