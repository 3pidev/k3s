# Using CiCd to build and Deploy

One of the software packages we made available for Raspberry Pi's is the CiCd solution of [Drone.io](https://drone.io). Before you install is check-out the step-1 of [this page](https://docs.drone.io/) on how to get you required intergration credentionals for you source system.

You can add the CiCd software by running
```bash
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/deploy.sh) \
-s cicd_type=**github** or **gitlab** \
-s cicd_client=**your client id** \
-s cicd_secret=**your client secret** \
install cicd
```
After installation CiCd will be available at https://cicd.sys.**YourDomain**

## Creating a CiCd Pipeline

On the [documentation site](https://docs.drone.io/) of Drone.io you will find more information on how to create a pipeline. You can take a look at our template for a kubernetes pipeline at the bottom of this document. 



We use a number of plugins to make our life simple


## Example of a Pipeline

```yaml
---
#####################################
# Example pipeline to build, publish
# and deploy to development
#####################################
kind: pipeline
type: kubernetes
name: development

platform:
  arch: arm64

steps:
  - name: test
    image: golang:1.12
    commands:
      - go test

  - name: code-analysis
    image: 3pidev/drone-sonar-plugin
    settings:
      sonar_host:
        from_secret: sonarqube_server
      sonar_token:
        from_secret: sonarqube_token

  #- name: build
  #  image: golang:1.12
  #  commands:
  #   - go build -o ./myapp

  #- name: publish
  #  image: plugins/docker
  #  settings:
  #   repo: 3pidev/k3scicd
  #registry: registry.3pi.dev
  #   tags: [ "${DRONE_COMMIT_SHA:0:7}","latest" ]
  #   username:
  #     from_secret: docker_username
  #   password:
  #     from_secret: docker_password

  - name: deploy
    image: sebt3/drone-kubectl:linux-arm64
    settings:
      cert:
        from_secret: k3s_cert
      token:
        from_secret: k3s_token
      kubectl:
        - apply -n cicd-dev -f deployment.yml
# We trigger only when we are pushed by webhook
trigger:
  event:
    - push

---
#####################################
# Example pipeline to
# deploy to staging
#####################################
kind: pipeline
type: kubernetes
name: staging

platform:
  arch: arm64

steps:
  - name: deploy
    image: sebt3/drone-kubectl:linux-arm64
    settings:
      cert:
        from_secret: k3s_cert
      token:
        from_secret: k3s_token
      kubectl:
        - apply -n cicd-stage -f deployment.yml

# Only run when we have a staging promote action
trigger:
  event:
    - promote
  target:
    - staging

---
#####################################
# Example pipeline to
# deploy to production
#####################################
kind: pipeline
type: kubernetes
name: production

platform:
  arch: arm64

steps:
  - name: deploy
    image: sebt3/drone-kubectl:linux-arm64
    settings:
      cert:
        from_secret: k3s_cert
      token:
        from_secret: k3s_token
      kubectl:
        - apply -n cicd-prod -f deployment.yml

# Only run when we have a production promote action
trigger:
  event:
    - promote
  target:
    - production

################################################
# Default section to get the system wide secrets
################################################
---
kind: secret
name: docker_username
get:
  path: docker
  name: username

---
kind: secret
name: docker_password
get:
  path: docker
  name: password

---
kind: secret
name: k3s_cert
get:
  path: k3s
  name: cert

---
kind: secret
name: k3s_token
get:
  path: k3s
  name: token

---
kind: secret
name: sonarqube_token
get:
  path: sonarqube
  name: token

---
kind: secret
name: sonarqube_server
get:
  path: sonarqube
  name: server

```
