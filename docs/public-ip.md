# External domain access
We want to make services available from the outside and therefor need to configure external DNS and internal forwarding. You can do this on your local network but we wanted our solution to be indepent of the internal network we use and therefor added two additional Mango portable wifi-router and a cloud server for public IP. 

## Wildcard Domain
During this installation we will create a number of Web Applications, who will be accessible by name only. This mean we need to buy/setup a domain name (ex: 3pi.dev). Within the DNS settings of you domain provider please point a wildcard towards the IP adress of your cluster-master or load balancer. In case you use our solution for a public ip address you can add a CNAME towards you DDNS name.

## Public IP with Cloud Server

We simplified the installation of the cloud server. First we created a free account at [dynu](https://www.dynu.com/) and setup a ddns entry for our server (3pidev.freeddns.org). Our services will be available on this address and you can use a CNAME in your domain registar to link your custom domain (in our case 3pi.dev). If you reinstall an existing cloud server you can provide also the private key of server and client.

Just configure the cloud server by logging in to the server and execute the following command.
```bash
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/deploy.sh) CLOUD "YOUR DDNS USER" "YOUR DDNS PASSWORD" ["DDNS_HOSTNAME","PRIVATE KEY SERVER","PRIVATE KEY CLIENT"]
```

## Configuring Mango 
We will use a [Mango](https://www.gl-inet.com/products/gl-mt300n-v2/) which supports wiregaurd vpn. First login into you mango following the vendor's instructions connect it to a wan, wifi or phone. 

Copy the config file created during installation of cloud server and shown during login or (/root/wiregaurd/wg0-client.conf) into the wiregaurd config section.

In the firewall sections add the following forwarded ports **80,443,220,221,222,9000 to 192.168.2.2**  

**NOTE**: It seems like that PostUp and PostDown are not working on the mango. So login to mango select: More Options->advanced and login again. Select network-firewall. Open tab *forwards* and add:

> wg-80 tcp wiregaurd 80 lan 192.168.2.2 80

> wg-433 tcp wiregaurd 433 lan 192.168.2.2 433

Select tab *General settings* and under general settings enable forward.
