### Installation of the Ubuntu OS

Before we can use our Pi's we need to install ubuntu 18.04(arm64) on our SD-Cards, for this we use the [raspbian imager](https://www.raspberrypi.org/downloads/). Just install the software, select the correct ubuntu os, insert sdcard in your computer and hit write. After installation of the OS put the SD-card into you PI and power-up. After a while you will be able to logon using (*ubuntu/ubuntu*) during the first logon you will be forced to set a new password. The script we use will do this changes for you.
