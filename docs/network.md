# Setup the edgerouter-x or Switch
We use an edgerouter to separate the our pi's from our internal network, but of course you can use a  Switch or connect your pi's directly to you local network. If you don't have an edgerouter just skip this section and connect the pi's directly to your network, you will need to know the IP addresses of you PI's.

Setup router for the first time by connecting a PC to eth0. Open a browser and connect to 192.168.1.1 and login with *ubnt/ubnt*. Now run form wizards the *basic setup* to configure your network
* Lan ports, Set network address on 192.168.2.1 and keep DHCP server checked
* Set the password for the ubnt user 
* Hit apply and the router will reboot
* Clear you static network settings
* Swap over you ethernet cable to port eth1 and open browser 192.168.2.1 and login
* cli->login after commit you will get a warning in the browser
```bash
configure
set system offload hwnat enable
set system offload ipsec enable
commit
save
exit
```

* Services->Actions->View Details and Limited dhcp-range to 191.168.2.100-223 allowing space for metalb on 224-250. 
* Setup the 4 pi's with static IP address 192.168.2.10-19 for all you pi's (k3s-0..9)
* Firewall/NAT -> add lan interface switch0 and add mapping needed to your pi's. We mapped ports 220..229 to the ssh port (22) of each pi. Also we mapped port 80,443 for to port 80,443 of to ip 192.168.2.224 allowing all http trafic to be handled by the loadblancer we will install.

# Configure Hardware load balancing
We used the [instruction](https://medium.com/@ipuustin/using-metallb-as-kubernetes-load-balancer-with-ubiquiti-edgerouter-7ff680e9dca3) but dot not get it working
* cli->login after commit you will get a warning in the browser
```bash
configure
set protocols bgp 64512 parameters router-id 192.168.2.1
set protocols bgp 64512 neighbor 192.168.2.10 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.11 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.12 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.13 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.14 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.15 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.16 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.17 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.18 remote-as 64512
set protocols bgp 64512 neighbor 192.168.2.19 remote-as 64512
set protocols bgp 64512 redistribute static
set protocols bgp 64512 maximum-paths ibgp 32
commit
save
exit
```