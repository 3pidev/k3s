#!/bin/bash
# ------------------------------------------------------------------
# [S.J.Hoeksma] deploy.sh
# Automated deployment script(s) for our 3pi kubernetes enviroment
# ------------------------------------------------------------------

# --- Contants ---------------------------------------------------

VERSION=0.1.0
SUBJECT=3pidevk3s
USAGE="Usage: deploy.sh -iephv action
 -h show this help
 -v show version number
 -s <setting> an option for one of the modules 
 -e <option> is excluded from standard install
 -i <option> is added to install
 -p password used for installation (can be encoded)
  
Currently we support the following installers:
 deploy  <list of master ips> <list of worker ips> : Configure machine(s) and deploy kubernetes
 clean   <list of master ips> <list of worker ips> : Clean install of kubernetes
 destroy <list of host ips>: Remove k3s installation of all machines
 cloud : Install the cloud server (DDNS_LOGIN,DDNS_PASSWORD,DDNS_HOSTNAME,[PRIVATE SERVER KEY],[PRIVATE CLIENT KEY])
 add <list of worker ips to add> <number of new host> : Add worker(s) to the cluster
 remove <list of workers ips to be removed> : Remove workers from cluster
 install <list of software packages> : Install the software packages
 uninstall <list of software packages> : Uninstall the software pacakages
 encode <value>: Encode a value
 decode <encode>: Decode a value
"
#URL towards the raw file of this project
RAW_REPO=https://gitlab.com/3pidev/k3s/-/raw/master
#List with file to exclude
exclude_list=""
#List with software to install
include_list=""
#List with settings
settings_list=""
#Password if used
password=""

# --- Options processing -------------------------------------------
if [ "$#" -eq "0" ] ;then
    echo "$USAGE"
    exit 1;
fi

while getopts ":s:i:p:e:vh" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "p") 
        if [ -z "$OPTARG" ] ;then
          read -s -p "Please enter password: " password
        else 
          if [[ $password == "*=" ]] ;then
            password=$(echo "$OPTARG" | base64 --decode)
          else
            password="$OPTARG"
          fi
        fi
      ;;
      "e")
        if [ -z "$exclude_list" ]; then
          exclude_list="$OPTARG"
        else
          exclude_list="${exculde_list} $OPTARG"
        fi  
        ;;
      "s")
        if [  -z "$settings_list" ]; then
          settings_list="$OPTARG"
        else
          settings_list="${settings_list} $OPTARG"
        fi  
        ;;
      "i")
        if [ -z "$include_list" ]; then
          include_list="$OPTARG"
        else
          include_list="${include_list} $OPTARG"
        fi  
        ;;  
      "h")
        echo "$USAGE"
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $(($OPTIND - 1))


# --- Locks -------------------------------------------------------
LOCK_FILE=/tmp/$SUBJECT.lock
if [ -f "$LOCK_FILE" ]; then
   echo "Script is already running"
   exit
fi

trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE

# --- Helper Functions --------------------------------------------

#Function to remotely load additional source files 
#param 1:Name of source
#param 2:location dir of source (default core)
#param 3:Raw Git repos of source (default $RAW_REPO)
sources=""
load_source(){
  local file
  local path=${2:-core}
  local list=$(echo $1 | sed 's/,/ /g')
  for file in $list ;do
    if [[ ${sources} != *"${file},"* ]] ;then #Check if file is not in sources
      sources="${sources}${file},"
      if [[ ${RAW_REPO} != *"https:"* ]] ;then #Check if file is local dir
        file="${3:-$RAW_REPO}/${path}/${file}.src"
        #echo "Loading source: $file"
        source ${file}
      else
        file="${3:-$RAW_REPO}/${path}/${file}.src"
        #echo "Loading source: $file"
        curl -sfL ${file}  -o /tmp/${SUBJECT}.src
        source /tmp/${SUBJECT}.src
        rm /tmp/${SUBJECT}.src
      fi
    fi  
  done 
}

# Function to get value from settings list
setting=""
setting_count=0
get_setting() {
  setting="" #Set the global value to empty
  local arr=(${settings_list})
  local i
  local values=""
  local found=0
  for i in "${arr[@]}" ;do
    local kv=(${i//=/ })
    if [ "$kv" == "${1}" ] ;then
      found=1
      if [ -z "$values" ] ;then
      values="${kv[1]}"
      else
        values="${values} ${kv[1]}"
      fi  
    fi
  done
  setting=$(echo $values | sed 's/,/ /g')
  setting_count=0
  for i in $setting ;do
    setting_count=$((setting_count + 1 ))
  done
  if [ "$found" -eq "0" ] ;then
    return 1
  fi
  return 0 
}

#Function to show time out message
timeout() {
 local secs=$1
  local msg="$2"
  if [ -n "$msg" ] ;then
    msg="${msg} "
  fi  
  printf "\r${msg}wait ${secs} "
  while [ $secs -gt 0 ]; do
    sleep 1
    secs=$((secs - 1 ))
    printf "\r${msg}wait ${secs} "
  done
  echo
}

#Get a json value 
#param 1: The key to find
#param 2: Array pos defaults to value (2)
function jsonValue() {
KEY=$1
num=${2:-2}
awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'$KEY'\042/){print $(i+1)}}}' | tr -d '"' | sed -n ${num}p
}

# --- Body --------------------------------------------------------
{
#Check if there was a overwrite for the repo
if get_setting "RAW_REPO" ;then
  RAW_REPO=$setting
fi

#Load the core constants for project
load_source "constants"

#Explode the lists 
include_list=$(echo $include_list | sed 's/,/ /g')
exculde_list=$(echo $exculde_list | sed 's/,/ /g')
list_1=$(echo $2 | sed 's/,/ /g')
list_2=$(echo $3 | sed 's/,/ /g')

#  SCRIPT LOGIC GOES HERE
case "$1" in 
  "SETUP_MACHINES") echo "Setup of machines ($2, $3)"
    load_source "setup_machines"
    if [ -n "$password" ] ;then
     setup_pwd "$list_1 $list_2" $password
   fi
    setup_machines "$list_1" "$list_2" "$4"
  ;;  

  "SETUP_SERVER") echo "Setup of servers ($2, $3)"
    load_source "setup_machines"
    setup_machines "$list_1" "$list_2" "$4"
  ;; 

  "ETCD_INSTALL") echo "Installing ETCD on Masters nodes ($2)"
   load_source "etcd"
   install_ectd "$list_1"
  ;;

  "ETCD_SETUP") echo "Setup ETCD on Masters nodes ($2)" 
    load_source "etcd"
    setup_etcd "$list_1"
  ;;

  "SETUP_MASTERS") echo "Setup and install of masters ($2)"
    load_source "k3s_masters"
    setup_masters "$list_1" 
  ;;  

  "INSTALL_MASTERS") echo "Install masters ($2)"
    load_source "k3s_masters"
    install_k3s_masters "$list_1" 
  ;;

  "install") echo "Installing software package(s)"
    load_source "software"
    if [ -n "$list_1" ]; then
      include_list=$list_1
    fi
    dir=${3:-software}
    install_software "" "$dir" ""
  ;;

  "uninstall") echo "Uninstalling software package(s)"
    load_source "software"
    if [ -n "$list_1" ]; then
      include_list=$list_1
    fi  
    dir=${3:-software}
    install_software "delete" "$dir" ""
  ;;

  "add") echo "Adding workers ($2)"
    load_source "setup_machines,k3s_workers"
    if [ -n "$password" ] ;then
     setup_first_pwd "$list_1" $password
    fi
    if [ -n "$3" ] ;then
     setup_machines "" "$list_1" "$3"
    fi
    install_k3s_workers "$list_1" 
  ;;

  "remove") echo "Removing workers ($2)"
    load_source "k3s_workers"
    uninstall_k3s_workers "$list_1"
  ;;

  "cloud") echo "Installing cloud server"
    load_source "setup_cloud_server" 
    setup_cloud_server "$list_1" "$list_2" ${4:-"$(hostname -s).freeddns.org"} "$5" "$6" 
  ;;

  "encode") read -s -p "Please enter secret to encode: " secret
    echo "$secret" | base64 
  ;;  

  "decode") echo "$2" | base64 --decode
  ;;

  "destroy") echo "Destroying cluster ($2)"
   load_source "destroy" "software"
   k3s_destroy "$2"
  ;;

  "deploy") echo "Starting fresh deployment ($2, $3)"
   starttime=$(date +%s)
   load_source "checks,setup_machines,k3s_masters,k3s_workers,software"
   if [ -n "$password" ] ;then
    #Change first login password
    setup_first_pwd "$list_1 $list_2" $password
   fi
   #Setup linux env
   setup_machines "$list_1" "$list_2" "$4"
   count=0
   for ip in $list_1 $list_2 ;do
     wait_for_internet $ip 
     count=$(( $count + 1 ))
   done  
   #Setup the master
   setup_masters "$list_1"
   #Setup the workers
   install_k3s_workers "$list_2"
   #Install all default packages
   install_software "" "software" ""
   endtime=$(date +%s)
   echo >&2 "Finished deployment kubernetes on $count machines, it took $((${endtime}-${starttime})) seconds."
   ;;

  "clean")   echo "Starting clean deployment ($2, $3)"
   starttime=$(date +%s)
   load_source "checks,k3s_masters,k3s_workers,software"
   #Wait for the host to be available
   count=0
   for ip in $list_1 $list_2 ;do
     wait_for_internet $ip 
     count=$(( $count + 1 ))
   done  
   #Setup the master
   setup_masters "$list_1"
   #Setup the workers
   install_k3s_workers "$list_2"
   #Install all default packages
   install_software "" "software" ""
   endtime=$(date +%s)
   echo >&2 "Finished deployment kubernetes on $count machines, it took $((${endtime}-${starttime})) seconds."
   ;;

  "test")
    echo "no Test"
  ;;

  *) echo "Unknown option ($1)"  
  echo "$USAGE"
  ;;

esac
}

