# 3Pi.dev - Kubernetes Cluster 

As 3pi.dev we love our raspberry pi's and wanted to see where they could support us in our actual development work. We decided that we use 3 pi's and some additional hardware components to create a (High Available) kuberenetes. The cluster will contain a fully operational ci/cd solution to build and deploy our applications. 

The hardware list (total cost ~€350,-)
* 3 x raspberry-pi 4(4Gb), with active cooling
* 3 x MicroSd card 32gb
* 3 x 20cm usb-c to usb cables
* 1 x EdgeRouter-X or Switch
* 4 x 20cm CAT5 cabling
* 1 x RavPower, 4 poort USB power supply

Optional
* 1 x Mango portable wifi router, wiregaurd to cloud account
* 1 x Cloud account (gce or vultr) to get public ip
* 3 x 128GB usb stick (for local storage)
 
![Basic Concept](https://miro.medium.com/max/2670/1*HTDfBECb6WCJh3usWGxEdg.png)

Before we can begin, we need to do some hardware configuration. 
* [Install Ubuntu 18.04 on MicroSd](docs/ubuntu.md)
* [Setup Network](docs/network.md)
* [Get Public Domain and Public IP](docs/public-ip.md)

# Creating a kubernetes cluster

We assume from this point on that you have inserted the ubuntu sd-cards in your pi's, setup edgerouter and mango.

To make the installation simple we have created one script which will do all installation and deployment work. You should specify your list of ips like this *"192.168.2.10 192.168.2.11"*. If you only specify one master, no HA environment is created.

We have added a usb to every PI for localstorage, during the setup of the PI we by default mount and format the first USB drive (if exists) to the */data/k3s_local* directory. If you want to prevent formating add *-o usb=noformat* or add -o usb=skip to skip this step fully.

**NOTE:** In case you allready have configured the devices, then don't use the option of the password, you will be prompted for the password on each machine.
```bash
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/deploy.sh) \
   -p <new ubuntu password and default ui password> \
   -s k3s_domain=<your domain>,k3s_email=<your domain email> \
   -s local_path_usb=<*skip* no usb config, *noformat* no format of usb> \
   -i <list of extra software to install> \
   -e <list of software to exculde> \
    deploy \
   "<list of master ips>" \
   "<list of worker ips>" 
```

During installation, you can decide which software packages you would like to install. All packages marked with "default" are inluded when you run install. You can add addtional packages/group by using the include-option *-i* or exclude by *-e*

|Package|Group|Description|
|:---|:---|:---|
|local_path|default|Change the local path to */data/k3s_local*|
|auto_upgrade|default|k3s will be automaticly updated|
|descheduler|default|Keep your nodes balanced|
|helm |default|Install helm on master node|
|metallb|default|Hardware loadbalancer|
|cert_manager|default|The certificate manager use cert-manager.io/cluster-issuer: *"letsencrypt-prod"*  or *"letsencrypt-staging"*| 
|dashboard||The kubernetes dashboard https://dashboard.sys.**k3s_domain**. Token required to login is in output|
|minio||Bucket storage solution https://minio.sys.**k3s_domain**. Default password option is used or use *-s minio_pwd="default passwd"* also you can change the avialable space *-s minio_size="10Gi"* or user *-s minio_usr="admin"*|
|backup||Backup your cluster environment with velero. We will install a local minio server to manage backup data, pointed to *-s backup_loc="/data/k3s_local/backup"*. We install a daily full and hourly backup. *-s backup_url="external url"* will configure external minio installation, *-s backup_dev="blockdevice"* if set blockdevice is formated and mounted|
|monitor||Monitoring of your kubernetes environment https://monitor.sys.**k3s_domain**.|
|registry||Private docker registry, Supported settings *-s registry_usr="admin" -s registry_pwd="default pwd"*|
|cicd||Install the [drone.io](https://drone.io) cicd solution https://cicd.sys.**k3s_domain**. You will need to set *-s cicd_type="github/gitlab/" -s cicd_client"client_id" -s cicd_secret="client_secret"*, when not set you will be prompted. Also we require you docker hub settings *-s docker_usr="docker hub user=3pi.dev" -s docker_pwd="docker hub password"*|
|sonarqube||Sonarqube code qaulity check. Will take 2.5Gb of memory so think about it before using. Default password is used or set by *-s sonarqube_pwd="default pwd"*. https://sonarqube.sys.**k3s_domain**. If you install sonarqube after install the sonarqube secret will be added to the cicd-build namespace. Also you could change the default plugins installed *-s sonarqube_plugins="javascript,typescript,yaml,php,python,web,go"*|
|mailslurper||Mailslurper is a fake email smtp server. Accessable trough https://mailslurper.com.**k3s_domain** (/service)|
|openfaas||Adds OpenFaas (Function as Services) https://openfaas.com.**k3s_domain** to your cluster. If you would like to use a password other than default use *-s openfaas_pwd="default pwd"*, if no default password is set you will be prompted|
|gluster||GlusterFS installation on minimal 2 nodes. Add during install *-s gluster_nodes="list of nodes"* and optional *-s gluster_dir="gluster dir"* **NOT WORKING ON K3S ARM64**|


# Manual Installation steps
You can also do the installation in manual steps. If you only specify one master no HA enviromnent will be created.

```bash
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/deploy.sh) -p changeme SETUP_MACHINES 192.168.2.10,192.168.2.11,192.168.2.12
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/deploy.sh) SETUP_MASTERS 192.168.2.10,192.168.2.11
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/deploy.sh) add 192.168.2.12
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/deploy.sh) install <empty=default or list of software pacakage>
```

# Let's Play
Now you have a Kubernetes cluster you also want to use it. To help you gettings started we provide some additional documentation and projects.

* [Understanding the concepts we use](docs/concepts.cmd)
* [Using CiCd to build and deploy](docs/cicd.md)
* [Adding Authorization and Authentication](docs/auth.md)
* [An API gateway to secure backend](docs/api-gateway.md)